import React from 'react';
import './App.css';
import Body from './components/BodyComponent';
import { Provider } from 'react-redux';
import { store } from './redux/configureStore';


function App() {
  return (
  	<Provider store={store}>
    <div id="wrapper">
      <Body />
    </div>
    </Provider>
  );
}
export default App;
