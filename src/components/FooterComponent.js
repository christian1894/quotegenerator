import React from 'react';

function Footer() {
  return (
    <footer id="footer">
						<ul className="copyright">
							<li><h2>Christian Páez</h2></li>
              <li><h2>Web Developer</h2></li>
						</ul>
					</footer>
  );
}

export default Footer;
