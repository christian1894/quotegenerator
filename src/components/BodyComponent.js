import React, {Component} from 'react';
import Footer from './FooterComponent';
import { fetchQuotes, fetchLoadingTest,
         fetchImage, fetchImageLoadingTest } from '../redux/ActionCreators'
import { connect } from 'react-redux';
import { FadeTransform, Loop, Fade } from 'react-animation-components';

const mapStateToProps = state => {
    return {
      quotes: state.quotes,
      image: state.image
    }
}

const mapDispatchToProps = (dispatch) => ({
  fetchQuotes: () => {dispatch(fetchQuotes())},
  fetchLoadingTest: () => {dispatch(fetchLoadingTest())},
  fetchImage: () => {dispatch(fetchImage())},
  fetchImageLoadingTest: () => {dispatch(fetchImageLoadingTest())}
});

//CONTENT COMPONENT
class RenderContent extends Component {

  constructor(props) {
    super(props);
    this.newQuote = this.newQuote.bind(this);
  }

  newQuote() {
  //this.props.fetchLoadingTest(); LOADING fetchLoadingTest
  this.props.fetchQuotes();
  /* this.props.fetchImage();
  this.props.setBackground(); */
  }
  render(){
    if(this.props.quotesLoading){
      return(
        <div className="col-12">
            <span className="fas fa-cog fa-spin fa-3x fa-fw text-primary"></span>
            <h5><strong>Loading Quote. . .</strong></h5>
        </div>
      );
    }

    /* if(this.props.imageLoading){
      return(
        <div className="col-12">
            <span className="fas fa-cog fa-spin fa-3x fa-fw text-primary"></span>
            <h5><strong>Loading Image. . .</strong></h5>
        </div>
      );
    } */

    else if (this.props.quotesErrMess) {
            return(
                <div className="col-12">
                    <span className="fas fa-times fa-3x fa-fw text-primary"></span>
                    <h5><strong>{this.props.quotesErrMess}</strong></h5>
                    <button onClick = {this.newQuote}>NEW QUOTE</button>
                </div>
                );
               }
    else {
      const twitterUrl = "https://twitter.com/intent/tweet?text=" + this.props.quote +
      " " + this.props.author + "&hashtags=quotes";
      const tumblrUrl = "https://www.tumblr.com/widgets/share/tool?posttype=quote&tags=quotes&caption=" + 
      this.props.author + "&content=" + this.props.quote + "&canonicalUrl=https://www.tumblr.com/buttons&shareSource=tumblr_share_button"


      return(
        <div>
            <FadeTransform in transformProps=
            {{ enterTransform: 'translateX(0px)',
             exitTransform: 'translateX(-100px)' }}>
          <header>
            <h1>{this.props.quote}</h1>
            <p>-{this.props.author}</p>
          </header>
          </FadeTransform>
          <FadeTransform in transformProps=
            {{ enterTransform: 'translateX(0px)',
             exitTransform: 'translateX(100px)' }}>
          <button onClick = {this.props.fetchQuotes}>NEW QUOTE</button>
          <div><br /></div>
          <footer>
            <ul className="icons">
              <li><a target = "_blank" 
              href= {twitterUrl}
               className="icon brands fa-twitter">Twitter</a></li>
              <li><a target = "_blank" href={tumblrUrl} className="icon brands fa-tumblr">Facebook</a></li>
            </ul>
              <Loop in>
                <Fade>
                  <p>Share this quote!</p>
                </Fade>
              </Loop>
          </footer>
          <Footer />
          </FadeTransform>
          </div>
          
      );
    }
  }
}
//CONTENT COMPONENT


//MAIN BODY COMPONENT
class Body extends Component {

  constructor(props) {
    super(props);
    this.setBackground = this.setBackground.bind(this);
    this.fetchImage = this.fetchImage.bind(this);
    /* this.changeData = this.changeData.bind(this); */
    this.state = {
      imageLoading: true, 
      image:''
    }
  }
  setBackground() {
  const url = "url('" + this.state.image + "')"
   document.body.style.backgroundImage = url;
 }
  fetchImage(){
  return fetch(`https://source.unsplash.com/random`)
  .then(response => {
    if (response.ok) {
        return response;
    }
    else {
        var error = new Error('Error ' + response.status + ': ' +
         response.statusText);
        error.response = response;
        throw error;
    }
},
error => {
    var errmess = new Error(error.message);
    throw errmess;
}) 
    .then(response => response.url)
    .then(url => this.setState({image: url}))
    .then(()=> this.setBackground())
    /* .then(()=> this.setState({imageLoading: false})) */
    .catch(error => console.log(error.message))
 }

  /* async changeData(){
  this.setState({imageLoading: true});
  
  console.log(this.state)
  const image = await this.setBackground();
  const quotes = await this.props.fetchQuotes(); 
  
 }
 */
 componentDidMount() {
  /* this.props.fetchLoadingTest(); */
  this.fetchImage();
  this.props.fetchQuotes(); 
}

    render(){
      const quote = this.props.quotes.quotes.content;
      const author = this.props.quotes.quotes.author;
        return (
			<section id="main">
      <RenderContent quote = {quote} author = {author}
      fetchQuotes = {this.props.fetchQuotes}
      fetchImage = {this.props.fetchImage}
      setBackground = {this.setBackground}
      fetchLoadingTest = {this.props.fetchLoadingTest}
      quotesErrMess = {this.props.quotes.quotesErrMess}
      quotesLoading = {this.props.quotes.quotesLoading} 
      changeData = {this.changeData}
      imageLoading = {this.state.imageLoading}
      />
      </section>);
      }
}
//MAIN BODY COMPONENT

export default connect(mapStateToProps, mapDispatchToProps)(Body)
