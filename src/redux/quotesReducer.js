import * as ActionTypes from './ActionTypes';

export const Quotes = (state = {
        quotesLoading: true,
        quotesErrMess: null,
        quotes: []
    }, action) => {
    switch(action.type) {
        case ActionTypes.ADD_QUOTES:
            return {...state, quotesLoading: false, quotesErrMess: null, quotes: action.payload};

        case ActionTypes.QUOTES_LOADING:
            return {...state, quotesLoading: true, quotesErrMess: null, quotes: []};

        case ActionTypes.QUOTES_FAILED:
            return {...state, quotesLoading: false, quotesErrMess: action.payload, quotes: []};

        default:
            return state;
    }
}
