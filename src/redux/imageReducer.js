import * as ActionTypes from './ActionTypes';

export const Image = (state = {
        imageLoading: true,
        imageErrMess: null,
        image: ""
    }, action) => {
    switch(action.type) {
        
        case ActionTypes.ADD_IMAGE:
            return {...state, imageLoading: false, imageErrMess: null, image: action.payload};

        case ActionTypes.IMAGE_LOADING:
            return {...state, imageLoading: true, imageErrMess: null, image: ""};

        case ActionTypes.IMAGE_FAILED:
            return {...state, imageLoading: false, imageErrMess: action.payload, image: ""};

        default:
            return state;
    }
}
