import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { Quotes } from './quotesReducer';
import { Image } from './imageReducer';

const ConfigureStore = () => {
    const store = createStore(
        combineReducers({
            quotes: Quotes,
            image: Image
        }),
        applyMiddleware(thunk, logger)
    );
    return store;
}
export const store = ConfigureStore();
