import * as ActionTypes from './ActionTypes';
import * as urls from './urls';

//QUOTES FETCH ACTIONS CREATORS

//LOADING TEST
export const fetchLoadingTest = () => (dispatch) => {
    dispatch(quotesLoading(true));
    setTimeout(() => {
        dispatch(fetchQuotes());
    }, 5000);
}
//LOADING TEST

export const fetchQuotes = () => (dispatch) => {
    dispatch(quotesLoading(true));
    return fetch(urls.quotesUrl)
        .then(response => {
            if (response.ok) {
                return response;
            }
            else {
                var error = new Error('Error ' + response.status + ': ' + response.statusText);
                error.response = response;
                throw error;
            }
        },
        error => {
            var quotesErrmess = new Error(error.message);
            throw quotesErrmess;
        })
        .then(response => response.json())
        .then(quotes => dispatch(addQuotes(quotes)))
        .catch(error => dispatch(quotesFailed(error.message)));
}

export const addQuotes = (quotes) => ({
    type: ActionTypes.ADD_QUOTES,
    payload: quotes
});

export const quotesLoading = () => ({
    type: ActionTypes.QUOTES_LOADING
});

export const quotesFailed = (quotesErrMess) => {
  return ({
      type: ActionTypes.QUOTES_FAILED,
      payload: quotesErrMess
  });
}

//QUOTES FETCH ACTIONS CREATORS


//IMAGE FETCH ACTIONS CREATORS

//LOADING TEST
export const fetchImageLoadingTest = () => (dispatch) => {
    dispatch(imageLoading(true));
    setTimeout(() => {
        dispatch(fetchImage());
    }, 4000);
}
//LOADING TEST

export const fetchImage = () => (dispatch) => {
    dispatch(imageLoading(true));
    return fetch(urls.imageUrl)
        .then(response => {
            if (response.ok) {
                return response;
            }
            else {
                var error = new Error('Error ' + response.status + ': ' + response.statusText);
                error.response = response;
                throw error;
            }
        },
        error => {
            var ImageErrmess = new Error(error.message);
            throw ImageErrmess;
        })
        .then(response => response.url)
        .then(image => dispatch(addImage(image)))
        .catch(error => dispatch(imageFailed(error.message)));
}

export const addImage = (image) => ({
    type: ActionTypes.ADD_IMAGE,
    payload: image
});

export const imageLoading = () => ({
    type: ActionTypes.IMAGE_LOADING
});

export const imageFailed = (imageErrMess) => {
  return ({
      type: ActionTypes.IMAGE_FAILED,
      payload: imageErrMess
  });
}

//IMAGE FETCH ACTIONS CREATORS
