
// quotes fetching action types
export const QUOTES_LOADING = 'QUOTES_LOADING';
export const QUOTES_FAILED = 'QUOTES_FAILED';
export const ADD_QUOTES = 'ADD_QUOTES';
// quotes fetching action types

// image fetching action types
export const IMAGE_LOADING = 'IMAGE_LOADING';
export const IMAGE_FAILED = 'IMAGE_FAILED';
export const ADD_IMAGE = 'ADD_IMAGE';
// image fetching action types